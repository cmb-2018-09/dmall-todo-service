#!/usr/bin/env bash

sed -i "s/IMAGE_TAG/$BUILD_NUMBER/g" k8s.yml
kubectl --kubeconfig $KUBE_CONFIG_FILE apply -f k8s.yml
