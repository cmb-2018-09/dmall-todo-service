FROM java:8u111-jre-alpine
VOLUME /tmp

ADD build/libs/todos.jar /work/app.jar
ADD run.sh /

ENTRYPOINT ["/run.sh"]
