#!/usr/bin/env bash

DOCKER_REGISTRY_SERVER=52.83.118.68

sudo docker login $DOCKER_REGISTRY_SERVER -u $DOCKER_USERNAME -p $DOCKER_PASSWORD

sudo docker build -t $DOCKER_REGISTRY_SERVER/dmall/todo-service:$BUILD_NUMBER .
sudo docker tag $DOCKER_REGISTRY_SERVER/dmall/todo-service:$BUILD_NUMBER $DOCKER_REGISTRY_SERVER/dmall/todo-service:latest

sudo docker push $DOCKER_REGISTRY_SERVER/dmall/todo-service:$BUILD_NUMBER
sudo docker push $DOCKER_REGISTRY_SERVER/dmall/todo-service:latest